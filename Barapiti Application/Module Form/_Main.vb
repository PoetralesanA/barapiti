﻿Namespace ModuleMain
    Module Main
#Region "Check pemilihan rows"
        Private Function DatagridNullSelect(ByVal DATAGRID As DataGridView) As Boolean
            Dim i As Integer = DATAGRID.SelectedRows.Count
            If i = 0 Then
                Return False
            Else
                Return True
            End If
        End Function
#End Region
#Region "Bagian ini berfungsi untuk Hapus data (tertentu/terpilih) yang ada pada datagrid"

        Sub DeletePengeluaran(ByVal dataPengeluaran As DataGridView, ByVal rowsCells As Integer)
            If DatagridNullSelect(dataPengeluaran) = False Then 'check select data 
                Pesan.Exclamation("Silahkan pilih data yang ingin dihapus", "null value")
            Else

                Dim TABLE As String, FIELD As String, VALUE As String 'Database Deklarasi
                VALUE = String.Empty

                TABLE = "pengeluaran"
                FIELD = "id_data"
                VALUE = dataPengeluaran.SelectedRows(rowsCells).Cells(rowsCells).Value.ToString ' Get Id Value
                If Pesan.YesNoMsg("Hapus data ID : " + VALUE, "") = True Then
                    If Database.Crud.Delete(TABLE, FIELD, VALUE) = True Then ' Check kondisi error + delete
                        dataPengeluaran.ClearSelection() 'clear select datagrid
                        Pesan.Information("Data berhasil dihapus..", "info")
                    End If
                End If
            End If
        End Sub
        Sub DeleteTabungan(ByVal dataTabungan As DataGridView, ByVal rowsCells As Integer)
            If DatagridNullSelect(dataTabungan) = False Then 'check select data
                Pesan.Exclamation("Silahkan pilih data yang ingin dihapus", "null value")

            Else
                Dim TABLE As String, FIELD As String, VALUE As String 'Database Deklarasi
                VALUE = String.Empty 'reset

                TABLE = "tabungan"
                FIELD = "id_data"
                VALUE = dataTabungan.SelectedRows(rowsCells).Cells(rowsCells).Value.ToString ' Get Id Value
                'MsgBox(id_data)

                If Pesan.YesNoMsg("Hapus data ID : " + VALUE, "") = True Then 'Check kondisi error + delete
                    If Database.Crud.Delete(TABLE, FIELD, VALUE) = True Then ' delete data
                        dataTabungan.ClearSelection() 'clear select datagrid
                        Pesan.Information("Data berhasil dihapus..", "info")
                    End If
                End If
            End If
        End Sub
#End Region
#Region "Menampilkan keseluruhan value pada tabungan dan pengeluaran"
        Function TampilkanTotalTabungan(ByVal datagrid As DataGridView)
            '//Menjumlahkan seluruh pengeluaran
            Return ModuleLogic.HitungTotal.indexCell(datagrid, 1)
        End Function
        Function TampilkanTotalPengeluaran(ByVal datagrid As DataGridView)
            '//Menjumlahkan seluruh pengeluaran
            Return ModuleLogic.HitungTotal.indexCell(datagrid, 2)
        End Function
#End Region
#Region "Mengambil waktu & tanggal saat ini"
        Public Function GetTanggal()
            Return DateConvert.Indonesia(Format(Convert.ToDateTime(Now.Date), "dddd-MM-yyyy"))
        End Function
        Public Function GetTime()
            Return Format(Convert.ToDateTime(Now.ToLongTimeString), "HH:mm-tt")
        End Function
#End Region
#Region "Menampilkan data pengeluaran dan tabungan pada datagrid"
        Sub ViewPengeluaran(ByVal viewData As DataGridView)
            ' view data
            Dim FIELD As String = String.Empty,
                Table As String = String.Empty

            FIELD = "id_data AS 'ID DATA', nama_barang AS 'Nama Barang', harga AS 'Harga Beli', tanggal_pengeluaran AS 'Tanggal beli', jam AS Jam"
            Table = "pengeluaran"
            Database.Crud.View(FIELD, Table, viewData)
        End Sub
        Function ViewTabungan(ByVal viewData As DataGridView) As Boolean
            'Check Ilegal User
            Dim FIELD As String = String.Empty,
                Table As String = String.Empty

            Security.CheckUserID(ModuleLogin.GetID, ModuleLogin.GetUser)

            ' view data
            FIELD = "id_data AS 'ID DATA', total_dana AS 'Total Dana', tanggal_input_kas AS 'Tanggal Input', Jam_Input AS 'Jam'"
            Table = "tabungan"
            If Database.Crud.View(FIELD, Table, viewData) = True Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region
        Sub GetUserID(ByVal ID As Label, ByVal UserLogin As Label)
            ID.Text = "ID : " + ModuleLogin.GetID
            UserLogin.Text = "User Login : " + ModuleLogin.GetUser
        End Sub
    End Module
End Namespace