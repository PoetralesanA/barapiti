﻿Namespace ModuleTabungan
    Module _FMtabungan
        Private Table As String, FIELD As String, Values As String 'Variable Crud
        Private table_autonumber As String, field_autonumber As String, InisialCode_autonumber As String ' Variable Autonumber
        Private Function SetAutoNumber()
            InisialCode_autonumber = "T" 'T = code tabungan

            table_autonumber = "tabungan"
            field_autonumber = "id_data"
            Return Database.AutoNumber(table_autonumber, field_autonumber, InisialCode_autonumber)
        End Function
        Function Tabung(ByVal JumlahTabung As String) As Boolean
            Dim Autonumber As String = SetAutoNumber()
            Dim CheckCrud As Boolean
            If Not Autonumber = String.Empty Then
                Table = "tabungan"
                FIELD = "id_data, id_user, total_dana, tanggal_input_kas, Jam_Input"
                Values = "'" & Autonumber & "','" & ModuleLogin._FMLogin.GetID & "','" & JumlahTabung & "','" & Database.ConvertDate.DateToDB() & "','" & Database.ConvertTime.TimeToDB & "'"
                If Pesan.YesNoMsg("Tambah Dana..?", "") = True Then
                    If Database.Crud.Insert(
                                            Table,
                                            FIELD,
                                            Values
                                            ) = True Then
                        Pesan.Information("Data Berhasil Ditambahkan..", "")
                        CheckCrud = True
                    Else
                        CheckCrud = False
                    End If
                End If
            End If
            Return CheckCrud
        End Function
    End Module
End Namespace