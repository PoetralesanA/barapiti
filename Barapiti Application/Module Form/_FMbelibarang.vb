﻿Namespace ModuleBeliBarang
    Module _FMbelibarang
        Private Table As String, FIELD As String, Values As String
        Private Function SetAutoNumber()
            Dim autonumber_inisialcode As String = "P" 'P=Code Pengeluaran

            Dim autonumber_table As String = "pengeluaran"
            Dim autonumber_field As String = "id_data"
            Return Database.AutoNumber(autonumber_table, autonumber_field, autonumber_inisialcode)
        End Function
        Sub inputPembelian(ByVal NamaBarang As TextBox, ByVal Harga As TextBox, ByVal Deskripsi As RichTextBox)
            If Not SetAutoNumber() = String.Empty Then
                Table = "pengeluaran"
                FIELD = "id_data, id_user, nama_barang, harga, tanggal_pengeluaran, jam, deskripsi"
                Values = "'" & SetAutoNumber() &
                        "','" & ModuleLogin._FMLogin.GetID &
                        "','" & NamaBarang.Text &
                        "','" & Harga.Text &
                        "','" & Database.ConvertDate.DateToDB() &
                        "','" & Database.ConvertTime.TimeToDB &
                        "', '" & Deskripsi.Text & "'"

                '"==== CRUD.INSERT Flow ===="'
                '/// Name Tables -> 
                '/// Columns -> 
                '/// Values ;
                '"=========================="'
                If Pesan.YesNoMsg("Tambahkan daftar pembelian..?" + vbNewLine + vbNewLine +
                                  "-----------------------------------------" + vbNewLine +
                                  "Nama Barang : " + NamaBarang.Text +
                                  vbNewLine +
                                  "Harga Barang : Rp." + Harga.Text +
                                  vbNewLine +
                                  "Deskripsi : " + Deskripsi.Text +
                                  vbNewLine +
                                  "-----------------------------------------",
                                  "Konfirmasi Pembelian") = True Then
                    If Database.Crud.Insert(
                                            Table,
                                            FIELD,
                                            Values
                                            ) = True Then
                        Pesan.Information("Daftar pembelian berhasil disimpan..", "Info")
                    End If
                End If

            End If
        End Sub
        Function AturDeskripsi(ByVal Choice As ComboBox, ByVal txtDeskripsi As RichTextBox) As Boolean
            If Choice.Text = "Yes" Then
                txtDeskripsi.Clear() ' hapus => - (kosong)
                txtDeskripsi.Enabled = True ' aktifkan text

                txtDeskripsi.Focus()
                Return True
            Else
                txtDeskripsi.Text = "-" 'tulis - (kosong)
                txtDeskripsi.Enabled = False 'matikan text
                Return False
            End If
        End Function
        Function LenghtDeskripsi(ByVal Deskripsi As RichTextBox, ByVal lenght As Integer) As Boolean
            If Deskripsi.Text.Length > lenght Then
                Return True
            Else
                Return False
            End If
        End Function
        Function DataKosong(ByVal NamaBarang As TextBox, ByVal Harga As TextBox) As Boolean
            If NamaBarang.Text = String.Empty Or Harga.Text = String.Empty Then
                Return True
            Else
                Return False
            End If
        End Function
    End Module
End Namespace