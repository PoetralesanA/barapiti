﻿Namespace ModuleLogin
    Module _FMLogin
        Public GetUser As String, GetID As String
        Private Sub ClearUserID()
            GetUser = String.Empty
            GetID = String.Empty
        End Sub
        Sub ShowPassword(ByVal checkbx As CheckBox, ByVal passwordtext As TextBox)
            If checkbx.Checked = True Then
                passwordtext.UseSystemPasswordChar = False
            Else
                passwordtext.UseSystemPasswordChar = True
            End If
        End Sub
        Function Authentication(ByVal username As String, ByVal password As String) As Boolean
            ClearUserID()
            Using conn As New MySqlConnection(GetConn)
                Using cmnd As New MySqlCommand("SELECT * FROM `user` WHERE username='" & username & "' AND password ='" & password & "'", conn)
                    Try
                        conn.Open()
                        readDT = cmnd.ExecuteReader()
                        If readDT.HasRows = True Then
                            readDT.Read()
                            GetID = readDT.Item("id_user")
                            GetUser = readDT.Item("username")
                            readDT.Close()
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        Pesan.Critical(ex.Message, "")
                        Application.Exit()
                    Finally
                        conn.Close()
                    End Try
                End Using
            End Using
            Return Nothing
        End Function
        Sub Check(ByVal username As TextBox, ByVal password As TextBox)
            If ModuleLogin.Authentication(username.Text, password.Text) = True Then
                Pesan.Information("Login Berhasil..", "")

                Main.Show()
                FMLogin.Close()
            Else
                username.Focus()
                Pesan.Critical("Username & Password Salah!", "")
            End If
        End Sub
    End Module
End Namespace