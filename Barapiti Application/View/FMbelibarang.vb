﻿Public Class FMbelibarang
    Private Sub tambahClick(sender As Object, e As EventArgs) Handles btnTambah.Click
        'MsgBox("miaw~", MsgBoxStyle.Information, "tes select")
        If ModuleBeliBarang._FMbelibarang.LenghtDeskripsi(txtDks, 120) = True Then 'hitung panjang text
            Pesan.Exclamation("Melebihi karakter yang telah ditentukan" + vbNewLine +
                              "Panjang deskripsi Maximal 120 karakter", "")
        Else
            If ModuleBeliBarang.DataKosong(txtNAB, txtHrg) = True Then 'Check data kosong
                Pesan.Exclamation("Data yang anda masukan belum lengkap" +
                                  vbNewLine +
                                  "Nama barang atau harga barang tidak boleh kosong",
                                  "Data belum lengkap")
            Else
                'Insert data
                ModuleBeliBarang._FMbelibarang.inputPembelian(txtNAB, txtHrg, txtDks)
                ResetValue()
            End If
        End If
    End Sub
    Private Sub ResetValue()
        Dim Yes As Integer = 0
        Dim No As Integer = 1

        txtNAB.Text = String.Empty ' Reset nama barang
        txtHrg.Text = String.Empty ' Reset harga barang
        cbxChoice.SelectedIndex = No ' Reset Choice

        txtNAB.Focus() ' focus ke text nama_barang
    End Sub

    Private Sub FMbelibarang_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Security.CheckUserID(ModuleLogin.GetUser, ModuleLogin.GetID)

        '// Text Watermark or placeholder
        SetWatermark(txtNAB, "Isi nama barang yang anda beli..")
        SetWatermark(txtHrg, "Isi harga yang telah anda keluarkan..")
        cbxChoice.SelectedIndex = 1 ' 1=No, 0=Yes
    End Sub
    Private Sub ComboBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxChoice.KeyPress
        ModuleEvent.WriteText.Disable(e)
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxChoice.SelectedIndexChanged
        If ModuleBeliBarang._FMbelibarang.AturDeskripsi(cbxChoice, txtDks) <> True Then
            btnTambah.Select()
        End If
    End Sub

    Private Sub txtHrg_KeyPress(sender As Object, txtHarga_ As KeyPressEventArgs) Handles txtHrg.KeyPress
        ModuleEvent.AccNumber.NumbericOnly(txtHarga_)
    End Sub
End Class