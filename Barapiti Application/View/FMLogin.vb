﻿Public Class FMLogin
    Private Sub pwdCheckChgd(sender As Object, e As EventArgs) Handles CBXpwdshow.CheckedChanged
        ModuleLogin.ShowPassword(CBXpwdshow, txtpassword)
    End Sub
    Private Sub LoginClick(sender As Object, e As EventArgs) Handles btnLogin.Click
        CheckForIllegalCrossThreadCalls = False
        btnLogin.Enabled = False
        ModuleLogin.Check(txtusername, txtpassword)
        btnLogin.Enabled = True
    End Sub
End Class