﻿Public Class FMTabungan
    Private TABLE As String, FIELD As String, InisialCode As String
    Private Sucessfully As Boolean = True
    Private Sub ResetValue()
        txtJumlah.Clear()
        txtJumlah.Focus()
    End Sub
    Private Sub Cick_Tabung(sender As Object, e As EventArgs) Handles btnTabung.Click
        If ModuleTabungan.Tabung(txtJumlah.Text) = Sucessfully Then
            ResetValue()
        End If
    End Sub
    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtJumlah.KeyPress
        ModuleEvent.AccNumber.NumbericOnly(e)
    End Sub

    Private Sub FMTabungan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Security.CheckUserID(ModuleLogin.GetUser, ModuleLogin.GetID)
    End Sub
End Class