﻿Class Pesan
    Public Shared Function Critical(ByVal val As String, ByVal title As String)
        Return MsgBox(val, MsgBoxStyle.Critical, title)
    End Function
    Public Shared Function Information(ByVal val As String, ByVal title As String)
        Return MsgBox(val, MsgBoxStyle.Information, title)
    End Function
    Public Shared Function Exclamation(ByVal val As String, ByVal title As String)
        Return MsgBox(val, MsgBoxStyle.Exclamation, title)
    End Function
    Public Shared Function YesNoMsg(ByVal val As String, ByVal title As String) As Boolean
        Dim ResultMsg As Boolean = False
        If MessageBox.Show(val, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            ResultMsg = True
        End If
        Return ResultMsg
    End Function
End Class