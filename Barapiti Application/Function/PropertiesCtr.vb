﻿Namespace DesignDatagrid

    Public Class PropertiesCtr

        Public Shared Sub DataGrid(ByVal Grid As DataGridView)
            With Grid
                .AllowUserToAddRows = False
                .AllowUserToDeleteRows = False
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
                .ReadOnly = True

                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                '----hitung row---
                'For row = 0 To Grid.RowCount - 1
                '    Grid.Rows(row).Cells(0).Value = row + 1
                'Next
            End With
        End Sub
    End Class
End Namespace
'Namespace DesignButton
'    Class PropertiesCtr
'        Public Shared Sub myButton(ByVal MYBUTTON As Button)

'        End Sub
'    End Class
'End Namespace