﻿Namespace DateConvert
    Module ConvtDate
        Public Function Indonesia(ByVal value As String) 'Manipulasi Tanggal
            Return Replace(Replace(Replace(Replace(Replace(Replace(
                    Replace(value,
                           "Sunday", "Minggu"),
                           "Monday", "Senin"),
                           "Tuesday", "Selasa"),
                           "Wednesday", "Rabu"),
                           "Thursday", "Kamis"),
                           "Friday", "Jumat"),
                           "Saturday", "Sabtu")
        End Function
    End Module
End Namespace
