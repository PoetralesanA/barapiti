﻿Public Class Main
    Dim i As Integer = 0
    Dim id_data As String
#Region "Bagian ini berfungsi untuk merefresh semua data, Baik itu dalam datagrid Maupun Label"
    Private Sub RefreshData(sender As Object, e As EventArgs) Handles RefreshAllData.Click
        If rfshTbgn() = True Then
            ModuleMain.ViewPengeluaran(dataPengeluaran) 'refresh pengeluaran 
            refreshLabelTabungANDpengeluaran()
            Pesan.Information("Refresh Berhasil..", "_::Information::_")
        End If
    End Sub
    Private Sub SegarkanData(sender As Object, e As EventArgs) Handles tabunganRefresh.Click
        RefreshAllData.PerformClick()
    End Sub
#End Region
#Region "Setting Date and Time"
    Private Sub GetJam()
        lblJam.Text = "Jam : " & ModuleMain.Main.GetTime()
    End Sub
    Private Sub GetTanggal()
        lblTanggal.Text = "Tanggal : " & ModuleMain.Main.GetTanggal()
    End Sub
#End Region
#Region "Bagian ini berfungsi untuk mengambil & menampilkan data pada saat pertama kali Aplikasi dijalankan"
    Private Sub LoadData()
        '--// Time (SUB PRIVATE)
        GetTanggal()
        GetJam()

        '--// Wartermark
        SetWatermark(txtSearch_tabungan, "Cari Data...")
        SetWatermark(txtSearch_Pengeluaran, "Cari Data...")

        '--// Mengambil ID dan User Pada Form Login
        ModuleMain.GetUserID(lbID, lbUser)

        '--//Menampilkan Datagird Tabungan
        ModuleMain.ViewTabungan(dataTabungan)
        ModuleMain.ViewPengeluaran(dataPengeluaran)

        '--// Hitung Total Tabungan
        refreshLabelTabungANDpengeluaran()

        '--// Properti pada datagrid
        PropertiesDatagrid()
    End Sub
#End Region
    Function rfshTbgn() As Boolean ' refresh grid tabungan
        Return ModuleMain.ViewTabungan(dataTabungan)
    End Function
    Private Sub Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadData()
    End Sub
    Private Sub refreshLabelTabungANDpengeluaran()
        '--// Hitung Total Tabungan
        lbleTbng.Text = "Tabungan : Rp." & ModuleMain.TampilkanTotalTabungan(dataTabungan)
        lblePglrn.Text = "Pengeluaran : Rp." & ModuleMain.TampilkanTotalPengeluaran(dataPengeluaran)
    End Sub
    Private Sub PropertiesDatagrid()
        '--//Pengaturan Datagrid
        DesignDatagrid.PropertiesCtr.DataGrid(dataTabungan)
        DesignDatagrid.PropertiesCtr.DataGrid(dataPengeluaran)
    End Sub
    Private Sub LoginClick(sender As Object, e As EventArgs) Handles btnTbg.Click
        FMTabungan.Show()
    End Sub

    Private Sub TambahTabunganToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahTabunganToolStripMenuItem.Click
        FMTabungan.Show()
    End Sub

    Private Sub btnBeliBarang(sender As Object, e As EventArgs) Handles btnBB.Click
        FMbelibarang.Show()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FMbelibarang.Show()
    End Sub
#Region "Private Project"
    Private Sub btnBkp_Click(sender As Object, e As EventArgs) Handles btnBkp.Click
        Pesan.Exclamation("Tahap Pengembangan~", "")
    End Sub

    Private Sub btnRsr_Click(sender As Object, e As EventArgs) Handles btnRsr.Click
        Pesan.Exclamation("Tahap Pengembangan~", "")
    End Sub

    Private Sub BtnLog_Click(sender As Object, e As EventArgs) Handles BtnLog.Click
        Pesan.Exclamation("Tahap Pengembangan~", "")
    End Sub

    Private Sub btnRpt_Click(sender As Object, e As EventArgs) Handles btnRpt.Click
        Pesan.Exclamation("Tahap Pengembangan~", "")
    End Sub
#End Region

    Private Sub HapusToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HapusToolStripMenuItem.Click
        ModuleMain.DeleteTabungan(dataTabungan, 0) 'datagrid, cell/rows index
    End Sub
    Private Sub ToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
        ModuleMain.DeletePengeluaran(dataPengeluaran, 0) ' nama grid, index delete
    End Sub
End Class