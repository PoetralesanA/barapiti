﻿Namespace ModuleLogic
    Module HitungTotal
        Private TMPint As Double
        ' Menghitung totalRows(integer) dalam gird, sesuai column(index) yang telah ditentukan
        Function indexCell(ByVal DataGrid As DataGridView, ByVal CellIndex As Integer)
            TMPint = 0
            Try
                For DataRows As Integer = 0 To DataGrid.Rows.Count - 1
                    TMPint += Val(DataGrid.Rows(DataRows).Cells(CellIndex).Value)
                Next
            Catch ex As Exception
                Pesan.Exclamation(ex.Message, "Kesalahan dalam pemilihan Column")
                Application.Exit()
            End Try
            Return TMPint
        End Function
    End Module
End Namespace