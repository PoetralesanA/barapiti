﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lbID = New System.Windows.Forms.Label()
        Me.lbUser = New System.Windows.Forms.Label()
        Me.lblJam = New System.Windows.Forms.Label()
        Me.lblTanggal = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.txtSearch_tabungan = New System.Windows.Forms.TextBox()
        Me.dataTabungan = New System.Windows.Forms.DataGridView()
        Me.contextTabungan = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TambahTabunganToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HapusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshAllData = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.txtSearch_Pengeluaran = New System.Windows.Forms.TextBox()
        Me.dataPengeluaran = New System.Windows.Forms.DataGridView()
        Me.contextPengeluaran = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tabunganRefresh = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.BtnLog = New System.Windows.Forms.Button()
        Me.btnRsr = New System.Windows.Forms.Button()
        Me.btnBkp = New System.Windows.Forms.Button()
        Me.btnRpt = New System.Windows.Forms.Button()
        Me.btnBB = New System.Windows.Forms.Button()
        Me.btnTbg = New System.Windows.Forms.Button()
        Me.lbleTbng = New System.Windows.Forms.Label()
        Me.lblePglrn = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dataTabungan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.contextTabungan.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dataPengeluaran, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.contextPengeluaran.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbID
        '
        Me.lbID.AutoSize = True
        Me.lbID.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbID.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbID.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbID.Location = New System.Drawing.Point(13, 4)
        Me.lbID.Name = "lbID"
        Me.lbID.Size = New System.Drawing.Size(41, 17)
        Me.lbID.TabIndex = 2
        Me.lbID.Text = "ID : 0"
        '
        'lbUser
        '
        Me.lbUser.AutoSize = True
        Me.lbUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbUser.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbUser.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbUser.Location = New System.Drawing.Point(13, 24)
        Me.lbUser.Name = "lbUser"
        Me.lbUser.Size = New System.Drawing.Size(149, 17)
        Me.lbUser.TabIndex = 3
        Me.lbUser.Text = "User Login : 571M Corp"
        '
        'lblJam
        '
        Me.lblJam.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblJam.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJam.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblJam.Location = New System.Drawing.Point(419, 391)
        Me.lblJam.Name = "lblJam"
        Me.lblJam.Size = New System.Drawing.Size(113, 20)
        Me.lblJam.TabIndex = 4
        Me.lblJam.Text = "Jam : HH:mm:ss"
        Me.lblJam.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTanggal
        '
        Me.lblTanggal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblTanggal.Font = New System.Drawing.Font("Segoe UI", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTanggal.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblTanggal.Location = New System.Drawing.Point(215, 391)
        Me.lblTanggal.Name = "lblTanggal"
        Me.lblTanggal.Size = New System.Drawing.Size(198, 20)
        Me.lblTanggal.TabIndex = 5
        Me.lblTanggal.Text = "Tanggal : dd/mm/yyyy"
        Me.lblTanggal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 61)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(521, 325)
        Me.TabControl1.TabIndex = 8
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.RadioButton4)
        Me.TabPage1.Controls.Add(Me.RadioButton3)
        Me.TabPage1.Controls.Add(Me.RadioButton2)
        Me.TabPage1.Controls.Add(Me.RadioButton1)
        Me.TabPage1.Controls.Add(Me.txtSearch_tabungan)
        Me.TabPage1.Controls.Add(Me.dataTabungan)
        Me.TabPage1.Location = New System.Drawing.Point(4, 29)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(513, 292)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Tabungan"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton4.Location = New System.Drawing.Point(440, 265)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(64, 19)
        Me.RadioButton4.TabIndex = 13
        Me.RadioButton4.Text = "Jumlah"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton3.Location = New System.Drawing.Point(375, 265)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(59, 19)
        Me.RadioButton3.TabIndex = 12
        Me.RadioButton3.Text = "Tahun"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton2.Location = New System.Drawing.Point(314, 265)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(55, 19)
        Me.RadioButton2.TabIndex = 11
        Me.RadioButton2.Text = "Bulan"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton1.Location = New System.Drawing.Point(233, 264)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(68, 19)
        Me.RadioButton1.TabIndex = 10
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Tanggal"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'txtSearch_tabungan
        '
        Me.txtSearch_tabungan.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch_tabungan.Location = New System.Drawing.Point(7, 263)
        Me.txtSearch_tabungan.Name = "txtSearch_tabungan"
        Me.txtSearch_tabungan.Size = New System.Drawing.Size(204, 23)
        Me.txtSearch_tabungan.TabIndex = 9
        '
        'dataTabungan
        '
        Me.dataTabungan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataTabungan.ContextMenuStrip = Me.contextTabungan
        Me.dataTabungan.Location = New System.Drawing.Point(6, 6)
        Me.dataTabungan.Name = "dataTabungan"
        Me.dataTabungan.Size = New System.Drawing.Size(501, 254)
        Me.dataTabungan.TabIndex = 8
        '
        'contextTabungan
        '
        Me.contextTabungan.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TambahTabunganToolStripMenuItem, Me.HapusToolStripMenuItem, Me.RefreshAllData})
        Me.contextTabungan.Name = "contextTabungan"
        Me.contextTabungan.Size = New System.Drawing.Size(176, 70)
        '
        'TambahTabunganToolStripMenuItem
        '
        Me.TambahTabunganToolStripMenuItem.Name = "TambahTabunganToolStripMenuItem"
        Me.TambahTabunganToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.TambahTabunganToolStripMenuItem.Text = "Tambah Tabungan"
        '
        'HapusToolStripMenuItem
        '
        Me.HapusToolStripMenuItem.Name = "HapusToolStripMenuItem"
        Me.HapusToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.HapusToolStripMenuItem.Text = "Hapus"
        '
        'RefreshAllData
        '
        Me.RefreshAllData.Name = "RefreshAllData"
        Me.RefreshAllData.Size = New System.Drawing.Size(175, 22)
        Me.RefreshAllData.Text = "Refresh"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.RadioButton5)
        Me.TabPage2.Controls.Add(Me.RadioButton6)
        Me.TabPage2.Controls.Add(Me.RadioButton7)
        Me.TabPage2.Controls.Add(Me.RadioButton8)
        Me.TabPage2.Controls.Add(Me.txtSearch_Pengeluaran)
        Me.TabPage2.Controls.Add(Me.dataPengeluaran)
        Me.TabPage2.Location = New System.Drawing.Point(4, 29)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(513, 292)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Pengeluaran"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton5.Location = New System.Drawing.Point(438, 265)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(64, 19)
        Me.RadioButton5.TabIndex = 18
        Me.RadioButton5.Text = "Jumlah"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton6.Location = New System.Drawing.Point(373, 265)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(59, 19)
        Me.RadioButton6.TabIndex = 17
        Me.RadioButton6.Text = "Tahun"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton7.Location = New System.Drawing.Point(312, 265)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(55, 19)
        Me.RadioButton7.TabIndex = 16
        Me.RadioButton7.Text = "Bulan"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Checked = True
        Me.RadioButton8.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.ForeColor = System.Drawing.Color.RoyalBlue
        Me.RadioButton8.Location = New System.Drawing.Point(232, 264)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(68, 19)
        Me.RadioButton8.TabIndex = 15
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Tanggal"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'txtSearch_Pengeluaran
        '
        Me.txtSearch_Pengeluaran.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch_Pengeluaran.Location = New System.Drawing.Point(7, 263)
        Me.txtSearch_Pengeluaran.Name = "txtSearch_Pengeluaran"
        Me.txtSearch_Pengeluaran.Size = New System.Drawing.Size(204, 23)
        Me.txtSearch_Pengeluaran.TabIndex = 14
        '
        'dataPengeluaran
        '
        Me.dataPengeluaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataPengeluaran.ContextMenuStrip = Me.contextPengeluaran
        Me.dataPengeluaran.Location = New System.Drawing.Point(6, 6)
        Me.dataPengeluaran.Name = "dataPengeluaran"
        Me.dataPengeluaran.Size = New System.Drawing.Size(501, 254)
        Me.dataPengeluaran.TabIndex = 9
        '
        'contextPengeluaran
        '
        Me.contextPengeluaran.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.tabunganRefresh})
        Me.contextPengeluaran.Name = "contextTabungan"
        Me.contextPengeluaran.Size = New System.Drawing.Size(188, 70)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(187, 22)
        Me.ToolStripMenuItem1.Text = "Tambah Pengeluaran"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(187, 22)
        Me.ToolStripMenuItem2.Text = "Hapus"
        '
        'tabunganRefresh
        '
        Me.tabunganRefresh.Name = "tabunganRefresh"
        Me.tabunganRefresh.Size = New System.Drawing.Size(187, 22)
        Me.tabunganRefresh.Text = "Refresh"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.BtnLog)
        Me.TabPage3.Controls.Add(Me.btnRsr)
        Me.TabPage3.Controls.Add(Me.btnBkp)
        Me.TabPage3.Controls.Add(Me.btnRpt)
        Me.TabPage3.Controls.Add(Me.btnBB)
        Me.TabPage3.Controls.Add(Me.btnTbg)
        Me.TabPage3.Location = New System.Drawing.Point(4, 29)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(513, 292)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Tools"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'BtnLog
        '
        Me.BtnLog.BackColor = System.Drawing.Color.OrangeRed
        Me.BtnLog.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnLog.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLog.ForeColor = System.Drawing.SystemColors.Control
        Me.BtnLog.Location = New System.Drawing.Point(247, 182)
        Me.BtnLog.Name = "BtnLog"
        Me.BtnLog.Size = New System.Drawing.Size(158, 109)
        Me.BtnLog.TabIndex = 6
        Me.BtnLog.Text = "Log"
        Me.BtnLog.UseVisualStyleBackColor = False
        '
        'btnRsr
        '
        Me.btnRsr.BackColor = System.Drawing.Color.DarkCyan
        Me.btnRsr.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnRsr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRsr.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRsr.ForeColor = System.Drawing.SystemColors.Control
        Me.btnRsr.Location = New System.Drawing.Point(404, 85)
        Me.btnRsr.Name = "btnRsr"
        Me.btnRsr.Size = New System.Drawing.Size(103, 206)
        Me.btnRsr.TabIndex = 5
        Me.btnRsr.Text = "Restore"
        Me.btnRsr.UseVisualStyleBackColor = False
        '
        'btnBkp
        '
        Me.btnBkp.BackColor = System.Drawing.Color.Indigo
        Me.btnBkp.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBkp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBkp.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBkp.ForeColor = System.Drawing.SystemColors.Control
        Me.btnBkp.Location = New System.Drawing.Point(94, 85)
        Me.btnBkp.Name = "btnBkp"
        Me.btnBkp.Size = New System.Drawing.Size(312, 97)
        Me.btnBkp.TabIndex = 4
        Me.btnBkp.Text = "Backup"
        Me.btnBkp.UseVisualStyleBackColor = False
        '
        'btnRpt
        '
        Me.btnRpt.BackColor = System.Drawing.Color.DarkTurquoise
        Me.btnRpt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnRpt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRpt.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRpt.ForeColor = System.Drawing.SystemColors.Control
        Me.btnRpt.Location = New System.Drawing.Point(94, 182)
        Me.btnRpt.Name = "btnRpt"
        Me.btnRpt.Size = New System.Drawing.Size(154, 109)
        Me.btnRpt.TabIndex = 3
        Me.btnRpt.Text = "Laporan"
        Me.btnRpt.UseVisualStyleBackColor = False
        '
        'btnBB
        '
        Me.btnBB.BackColor = System.Drawing.Color.SlateBlue
        Me.btnBB.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBB.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBB.ForeColor = System.Drawing.SystemColors.Control
        Me.btnBB.Location = New System.Drawing.Point(6, 85)
        Me.btnBB.Name = "btnBB"
        Me.btnBB.Size = New System.Drawing.Size(90, 204)
        Me.btnBB.TabIndex = 2
        Me.btnBB.Text = "Beli Barang"
        Me.btnBB.UseVisualStyleBackColor = False
        '
        'btnTbg
        '
        Me.btnTbg.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.btnTbg.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTbg.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTbg.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTbg.ForeColor = System.Drawing.SystemColors.Control
        Me.btnTbg.Location = New System.Drawing.Point(6, 1)
        Me.btnTbg.Name = "btnTbg"
        Me.btnTbg.Size = New System.Drawing.Size(501, 85)
        Me.btnTbg.TabIndex = 1
        Me.btnTbg.Text = "Tabungan"
        Me.btnTbg.UseVisualStyleBackColor = False
        '
        'lbleTbng
        '
        Me.lbleTbng.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbleTbng.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbleTbng.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbleTbng.Location = New System.Drawing.Point(274, 4)
        Me.lbleTbng.Name = "lbleTbng"
        Me.lbleTbng.Size = New System.Drawing.Size(267, 20)
        Me.lbleTbng.TabIndex = 9
        Me.lbleTbng.Text = "Tabungan : Rp. $$$"
        Me.lbleTbng.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblePglrn
        '
        Me.lblePglrn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblePglrn.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblePglrn.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblePglrn.Location = New System.Drawing.Point(278, 24)
        Me.lblePglrn.Name = "lblePglrn"
        Me.lblePglrn.Size = New System.Drawing.Size(263, 20)
        Me.lblePglrn.TabIndex = 10
        Me.lblePglrn.Text = "Pengeluaran : Rp. $$$"
        Me.lblePglrn.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(545, 418)
        Me.Controls.Add(Me.lblePglrn)
        Me.Controls.Add(Me.lbleTbng)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.lblTanggal)
        Me.Controls.Add(Me.lblJam)
        Me.Controls.Add(Me.lbUser)
        Me.Controls.Add(Me.lbID)
        Me.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aplikasi Barapti"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dataTabungan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.contextTabungan.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dataPengeluaran, System.ComponentModel.ISupportInitialize).EndInit()
        Me.contextPengeluaran.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbID As System.Windows.Forms.Label
    Friend WithEvents lbUser As System.Windows.Forms.Label
    Friend WithEvents lblJam As System.Windows.Forms.Label
    Friend WithEvents lblTanggal As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents dataTabungan As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents dataPengeluaran As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents btnTbg As System.Windows.Forms.Button
    Friend WithEvents btnRsr As System.Windows.Forms.Button
    Friend WithEvents btnBkp As System.Windows.Forms.Button
    Friend WithEvents btnRpt As System.Windows.Forms.Button
    Friend WithEvents btnBB As System.Windows.Forms.Button
    Friend WithEvents lbleTbng As System.Windows.Forms.Label
    Friend WithEvents lblePglrn As System.Windows.Forms.Label
    Friend WithEvents BtnLog As System.Windows.Forms.Button
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents txtSearch_tabungan As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Friend WithEvents txtSearch_Pengeluaran As System.Windows.Forms.TextBox
    Friend WithEvents contextTabungan As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents TambahTabunganToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HapusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshAllData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents contextPengeluaran As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tabunganRefresh As System.Windows.Forms.ToolStripMenuItem

End Class
