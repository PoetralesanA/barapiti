﻿Namespace Database
    Module ConvertDate
        Function DateToDB()
            Return Format(Convert.ToDateTime(Now.Date), "yyyy-MM-dd")
        End Function
    End Module
    Module ConvertTime
        Function TimeToDB()
            Return Format(Convert.ToDateTime(Now.ToLongTimeString), "HH:mm:ss")
        End Function
    End Module

    Module Crud
        Dim GrabID As Integer = 0
        ''' <summary>
        ''' INSERT DATABASE
        ''' </summary>
        ''' <param name="TABLE"></param>
        ''' <param name="COLUMN"></param>
        ''' <param name="VALUES"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function Insert(ByVal TABLE As String, ByVal COLUMN As String, ByVal VALUES As String) As Boolean
            Dim CheckedCRUD As Boolean = False
            'INSERT INTO `tabungan`(`id_user`, `total_dana`, `tanggal_input_kas`) VALUES ([value-1],[value-2],[value-3])
            Using conn As New MySqlConnection(GetConn)
                Using cmnd As New MySqlCommand("INSERT INTO " + TABLE + "(" & COLUMN & ") VALUES (" + VALUES + ")", conn)
                    Try
                        conn.Open()
                        cmnd.ExecuteNonQuery()
                        CheckedCRUD = True
                    Catch ex As Exception
                        Pesan.Critical(ex.Message, "")
                    Finally
                        conn.Close()
                    End Try
                End Using
            End Using
            Return CheckedCRUD
        End Function
        'Function Edit(ByVal query As String)

        'End Function
        Function Delete(ByVal TABLE As String, ByVal FIELD As String, ByVal VALUE As String) As Boolean
            ' DELETE FROM `pengeluaran` WHERE 0
            Dim CheckCRUD As Boolean = False
            Using conn As New MySqlConnection(GetConn)
                Using cmnd As New MySqlCommand("DELETE FROM " & TABLE & " WHERE " & FIELD & "='" & VALUE & "'", conn)
                    Try
                        conn.Open()
                        cmnd.ExecuteNonQuery()
                        CheckCRUD = True
                    Catch ex As Exception
                        Pesan.Critical(ex.Message, "")
                    Finally
                        conn.Close()
                    End Try
                End Using
            End Using
            Return CheckCRUD
        End Function
        Function View(ByVal FIELD As String, ByVal TABLENAME As String, ByVal datagridview As DataGridView) As Boolean
            Using conn As New MySqlConnection(GetConn)
                Using dataAdapter As New MySqlDataAdapter("SELECT " & FIELD & " FROM " & TABLENAME & "", conn)
                    Using dataTable As New DataTable
                        Try
                            dataAdapter.Fill(dataTable)
                            datagridview.DataSource = dataTable
                            Return True
                        Catch ex As Exception
                            Pesan.Critical(ex.Message, "")
                            Return False
                        Finally
                            conn.Close()
                        End Try
                    End Using
                End Using
            End Using
        End Function
        Function AutoNumber(ByVal TABLE As String, FIELD As String, ByVal InisialCode As String) As String
            Dim Text As String = String.Empty

            If InisialCode.ToString.Length <> 1 Then
                Pesan.Critical("Panjang karakter melebihi fungsi yang telah ditentukan" & vbNewLine &
                               "Maximal panjang Inisial Code = 1", "Statement ditolak!")
            Else
                Using conn As New MySqlConnection(GetConn)
                    Using Cmnd As New MySqlCommand("SELECT * FROM `" & TABLE & "` WHERE `" & FIELD & "` in (SELECT MAX(" & FIELD & ") FROM `" & TABLE & "`)", conn)
                        Try
                            conn.Open()
                            readDT = Cmnd.ExecuteReader
                            readDT.Read()
                            If readDT.HasRows Then
                                GrabID = Microsoft.VisualBasic.Mid(readDT.GetString(FIELD), 8) ' Ambil Value
                                GrabID += 1 ' Hitung 1
                                Text = "ID-" & InisialCode & "" & GrabID.ToString("D5") '000000
                            Else
                                Text = "ID-" & InisialCode & "00001"
                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        Finally

                            readDT.Close()
                            conn.Close()
                        End Try
                    End Using
                End Using
            End If
            Return Text
        End Function
    End Module
End Namespace